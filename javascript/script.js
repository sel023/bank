
let numberOfHoursWorkedSinceLastRaise = 0

let hourlyWages = 10

const bank = {
    funds: 0
}

const work = {
    currentPay: 0
}

let laptops; 
fetch('https://noroff-komputer-store-api.herokuapp.com/computers')
    .then(function(u){ return u.json();})
    .then(function(json){laptops = json;})

function workToPay(){
    console.log("working")
    if(numberOfHoursWorkedSinceLastRaise > 10){
        hourlyWages += 10
        numberOfHoursWorkedSinceLastRaise = 0
       }
    work.currentPay += hourlyWages
    numberOfHoursWorkedSinceLastRaise++
    document.getElementById("payout").innerHTML = `Current Payout: ${work.currentPay} kr`
}

function updateLaptopsWithinBuyingRange(){
    let list = document.getElementById("laptopsWithinBuyingRange")
    list.innerHTML = ""
    let nr = 0
    for(var laptop of laptops){
        if(bank.funds >= laptop.price){
            list.innerHTML += `<li><small>${laptop.title}</small></li>`
            nr++
        }
    }
    if(nr == 0){
        document.getElementById("afford").innerHTML = `<p>Can't afford any Laptops</p>`
    }else{
        document.getElementById("afford").innerHTML = `<p>Laptops you can afford</p>`
    }

}

function PopulateSelectBox(){
    let list = document.getElementById("select")
    if(list.childElementCount == 0){
    for(var laptop of laptops){
        list.innerHTML += `<option value =${laptop.id}>${laptop.title}</option>`
    }
}
}
function SelectedLaptop(id){
    let laptop = laptops[parseInt(id.value)-1]
    let toBePopulatedLaptop = document.getElementById("currentLaptop")
    toBePopulatedLaptop.innerHTML = `<p class="mt-2">${laptop.title}</p><p>Price: ${laptop.price} KR</p>`

    let komputerHeader = document.getElementById("komputerName")
    komputerHeader.innerHTML = `${laptop.title}`

    let toBePopulatedLaptopSpecs = document.getElementById("displayKomputer")
    toBePopulatedLaptopSpecs.innerHTML = ""
    for(var spec of laptop.specs){
        toBePopulatedLaptopSpecs.innerHTML += `${spec}. `
    }    

    let buyKomputer = document.getElementById("buy")
    buyKomputer.innerHTML = `<button class="btn btn-secondary btn-sm mt-3" onclick="buyComputer(${laptop.id})">Buy for ${laptop.price} kr</button>`
}

function withdrawPay(){
    if(work.currentPay != 0){
        bank.funds += work.currentPay
        work.currentPay = 0
        document.getElementById("payout").innerHTML = `Current Balance: 0 kr`
        document.getElementById("balance").innerHTML = `Current Balance: ${bank.funds} kr`
        updateLaptopsWithinBuyingRange()
    }
}

function buyComputer(id){
    let laptop = laptops[id-1]
    if(laptop.price <= bank.funds){
        bank.funds -= laptop.price 
        document.getElementById("balance").innerHTML = `Current Balance: ${bank.funds} kr` 
        alert(`Congratulations! you just bought ${laptop.title} for the small sum of ${laptop.price}`)    
    }else{
        alert("Not Enough Money you fool!")
    }

}

function getLoan(){
    alert("Sorry, your father can't give you a small loan of a million Kr")
}

